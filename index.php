<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Labomovies | Your Number One Movie Search Directory</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/slick/slick.css" rel="stylesheet">
    <link href="css/plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Steven Kasumba</strong>
                             </span> <span class="text-muted text-xs block">Software Developer </span> </span> </a>
                    </div>
                    <div class="logo-element">
                        LM
                    </div>
                </li>
                <li>
                    <a href="index.php"><i class="fa fa-th-large"></i> <span class="nav-label">Labomovies</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="index.php">Search Movie</a></li>
                        <!--<li><a href="previous.html">View Previous Searches</a></li>-->
                    </ul>
                </li>
                
            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Welcome to Labomovies.</span>
                    </li>
                    <li>
                        <a href="login.html">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Labomovie Search Directory</h2>
                <ol class="breadcrumb">
                    <li class="active">
                        <strong>Search for a movie</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">

            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox product-detail">
                        <div class="ibox-content">
                            <div class="row m-b-sm m-t-sm">
                                <div class="col-md-1">
                                    <a type="button" href="index.php" class="btn btn-white btn-sm" ><i class="fa fa-refresh"></i> Refresh</a>
                                </div>
                                <div class="col-md-11">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search Movie" id="title" class="input-sm form-control" > 
                                        <span class="input-group-btn">
                                            <button onclick="checkAPI()" type="button" class="btn btn-sm btn-primary"> Go!</button> 
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="product-images">
                                        <div>
                                            <div class="image-imitation">
                                                <img height="20%" width="100%" id="poster_image" src="">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-7">

                                    <h2 class="font-bold m-b-xs" id="title1">
                                    </h2>
                                    <div class="m-t-md">
                                        <h2 class="product-main-price">Genre <small class="text-muted" id="genre"></small> </h2>
                                    </div>
                                    <small>Year: <small id="year"> </small> </small>
                                    <hr>

                                    <h4>Plot</h4>

                                    <div class="small text-muted" id="plot">
                                        <? plot ?>
                                    </div>
                                    <dl class="small m-t-md">
                                        <dt>Director</dt>
                                        <dd id="director"><? director ?></dd>
                                        <dt>Writer</dt>
                                        <dd id="writer"><? writer ?></dd>
                                        <dt>Actors</dt>
                                        <dd id="actors"><? actors ?></dd>
                                        <dt>Language</dt>
                                        <dd id="language"><? language ?></dd>
                                        <dt>Country</dt>
                                        <dd id="country"><? Country ?></dd>
                                    </dl>
                                    <hr>

                                    <div>
                                        <dl class="small m-t-md">
                                            <dt>Awards</dt>
                                            <dd id="awards"><? awards ?></dd>
                                            <dt>Production</dt>
                                            <dd id="production"><? production ?></dd>
                                            <dt>Website</dt>
                                            <dd id="website"><? website ?></dd>
                                        </dl>
                                    </div>
                                    <hr>

                                    <table class="table table-bordered white-bg">
                                        <thead>
                                        <tr>
                                            <th>Graph</th>
                                            <th>Type</th>
                                            <th>Count</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td>
                                                    <span class="pie" id="metascore">6/9</span>
                                                </td>
                                                <td>
                                                    <code>Metascore</code>
                                                </td>
                                                <td>
                                                    <code id="metascore_count"><? Metascore ?></code>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="pie" id="imdbRating">2/5</span>
                                                </td>
                                                <td>
                                                    <code>imdbRating</code>
                                                </td>
                                                <td>
                                                    <code id="imdbRating_count"><? imdbRating ?></code>
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td>
                                                    <span class="pie" id="imdbVotes">1/4</span>
                                                </td>
                                                <td>
                                                    <code>imdbVotes</code>
                                                </td>
                                                <td>
                                                    <code id="imdbVotes_count"><? imdbVotes ?></code>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <hr>
<?php /*
                                    <div class="widget lazur-bg p-xl">
                                        <h2>
                                            Ratings
                                        </h2>
                                        <ul class="list-unstyled m-t-md">
                                            <li>
                                                <span class="fa fa-envelope m-r-xs"></span>
                                                <label>Email:</label>
                                                mike@mail.com
                                            </li>
                                            <li>
                                                <span class="fa fa-home m-r-xs"></span>
                                                <label>Address:</label>
                                                Street 200, Avenue 10
                                            </li>
                                            <li>
                                                <span class="fa fa-phone m-r-xs"></span>
                                                <label>Contact:</label>
                                                (+121) 678 3462
                                            </li>
                                        </ul>

                                    </div> */ ?>
                                </div>
                            </div>

                        </div>
                        <div class="ibox-footer">
                            <span class="pull-right">
                                From omdbapi - <i class="fa fa-clock-o"></i> 14.04.2016 10:04 pm
                            </span>
                            The Biggest And Only Search Directory
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer">
            <div>
                <strong>Copyright</strong> Labomovies &copy; 2019
            </div>
        </div>
    </div>
</div>



<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<!-- slick carousel-->
<script src="js/plugins/slick/slick.min.js"></script>

<!-- Peity -->
<script src="js/plugins/peity/jquery.peity.min.js"></script>

<!-- Peity demo data -->
<script src="js/demo/peity-demo.js"></script>


<script>
    $(document).ready(function(){


        $('.product-images').slick({
            dots: true
        });

    });



</script>
 
<script>

    function checkAPI() {
  
        var movie = $("#title").val();
        var omdbAPI = "https://www.omdbapi.com/?apikey=64ce79b1&plot=full";
            $.getJSON( omdbAPI, {
                t: movie
            })
        .done(function( data ) {
            /*.each( data.items, function( i, item ) {
                $( "<img>" ).attr( "src", item.media.m ).appendTo( "#images" );
                if ( i === 3 ) {
                  return false;
                }
            });*/
            var image = data.Poster;
            var title = data.Title;
            if (title !== "undefined") {

                if (image !== "N/A") {
                    $('#poster_image').attr('src', image);
                }

                //$.cookie("data", JSON.stringify(data));

                var genre = data.Genre;
                var year = data.Year;
                var plot = data.Plot;
                var director = data.Director;
                var writer = data.Writer;
                var actor = data.Actors;
                var lang = data.Language;
                var country = data.Country;
                var awards = data.Awards;
                var pdtn = data.Production;
                var website = data.Website;
                
                document.getElementById("title1").innerHTML = title;
                document.getElementById("genre").innerHTML = genre;
                document.getElementById("year").innerHTML = year;
                document.getElementById("plot").innerHTML = plot;
                document.getElementById("director").innerHTML = director;
                document.getElementById("writer").innerHTML = writer;
                document.getElementById("actors").innerHTML = actor;
                document.getElementById("language").innerHTML = lang;
                document.getElementById("country").innerHTML = country;
                document.getElementById("awards").innerHTML = awards;
                document.getElementById("production").innerHTML = pdtn;
                document.getElementById("website").innerHTML = website;

                var metascore = data.Metascore;
                var imdbRating = data.imdbRating;
                var imdbVotes = data.imdbVotes;
                document.getElementById("metascore").innerHTML = metascore + "/100";
                document.getElementById("imdbRating").innerHTML = imdbRating + "/10";
                document.getElementById("imdbVotes").innerHTML = imdbVotes;

                document.getElementById("metascore_count").innerHTML = metascore;
                document.getElementById("imdbRating_count").innerHTML = imdbRating;
                document.getElementById("imdbVotes_count").innerHTML = imdbVotes;

            }else{
                $('#poster_image').attr('src', "");
            }
            console.log(movie);
            console.log(data);
            //console.log(JSON.parse($.cookie("data")));
        });
    }
</script>
</body>

</html>
