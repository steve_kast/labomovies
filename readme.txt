An OMDB REST API that allows a user to:

1. View and filter movies
2. See a dashboard that shows useful information about the movie.

